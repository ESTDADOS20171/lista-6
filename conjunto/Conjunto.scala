package conjunto

object Conjunto {
  def criar[E >: Null](m: Mapeamento[E], e: E*): Conjunto[E] = {
    var c = new Conjunto[E](m);
    if (e != null) {
      e.foreach { x => c = c + x };
    }
    return c;
  }
};

class Conjunto[E >: Null](m: Mapeamento[E], b: Int = 0) {

  private var bits = b;

  /* complemento */
  def unary_~ = new Conjunto[E](m, ~this.bits);

  /* inserir */
  def +(e: E) = new Conjunto[E](m, bits | m > (e));
  
  /* remover */
  def -(e:E) = new Conjunto[E](m, bits & ~(~bits | m > (e)));

  /* uniao */
  def |(c: Conjunto[E]) = new Conjunto[E](m, this.bits | c.bits);

  /* intersecao */
  def &(c: Conjunto[E]) = new Conjunto[E](m, this.bits & c.bits);

  /* subtracao [de conjunto] */
  def \(c: Conjunto[E]) = new Conjunto[E](m, this.bits & (~c.bits));

  /* esta contido */
  def c(c: Conjunto[E]): Boolean = (this \ c).bits == 0;

  /* numero de elementos */
  def tamanho(): Int = Integer.bitCount(bits & m.mask);

  /* existe */
  def E(valor: E): Boolean = (this.bits & m > (valor)) != 0;

  /* igual */
  def ==(c: Conjunto[E]): Boolean = ((this c (c)) && (c.tamanho() == this.tamanho()));

  override def toString(): String = {

    if (bits == 0) "{}"

    var arr = new Array[String](0);

    for (i <- 0 until 31) {
      var e = m < (1 << i & bits);

      if (e != null) {
        arr = arr :+ e.toString();
      }
    }

    return arr.mkString("{", ", ", "}");

  }

}