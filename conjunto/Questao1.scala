package conjunto

object Questao1 extends App {

  // mapeamento dos elementos de bits
  var m = new Mapeamento[String](
    "Argetina", "Brasil", "Canada", "Dinamarca",
    "Estados Unidos", "Franca", "Grecia", "Holanda",
    "Italia", "Japao", "Luxemburgo",
    "Mexico", "Nova Zenlandia", "Omao", "Portugal",
    "Quenia", "Reino Unido", "Suecia", "Tailandia",
    "Uruguai", "Vietna", "Zambia");

  println("Mapeamento bit: " + m + "\n\n");

  println("[criar]Criar conjunto America Sul: \"Argetina\", \"Brasil\", \"Uruguai\"");
  var america_sul = Conjunto.criar(m, "Argetina", "Brasil");
  println(america_sul + "\n\n")
  println

  println("[inserir]Inserir no conjunto America Sul: \"Uruguai\"");
  america_sul = america_sul + "Uruguai";

  println(america_sul + "\n\n")
  println
  println("[criar]Criar conjunto America Norte: \"Estados Unidos\", \"Mexico\", \"Canada\"");

  var america_norte = Conjunto.criar(m, "Estados Unidos", "Mexico", "Canada");
  println(america_sul + "\n\n");
  println
  println("[uniao]Unir conjunto America Sul e conjunto America Norte");

  var america = america_sul | america_norte;
  println(america + "\n\n");
  println
  println("[criar]Criar conjunto Europa:  \"Franca\", \"Dinamarca\", \"Grecia\", \"Italia\", \"Holanda\", \"Luxemburgo\", \"Portugal\", \"Reino Unido\", \"Suecia\"");

  var europa = Conjunto.criar(m, "Franca", "Dinamarca", "Grecia", "Italia", "Holanda", "Luxemburgo", "Portugal", "Reino Unido", "Suecia");

  println(europa + "\n\n");
  println

  println("[remover]Paises da Uniao Europeia (Europa - Reino Unido)");
  var uniao_europeia = europa - "Reino Unido";

  println(uniao_europeia + "\n\n");
  println

  println("[complemento]Paises que nao sao da Europa")
  println(~europa + "\n\n"); ;
  println

  println("[complemento e diferenca]Paises que nao sao da Europa e nem da America")
  println((~europa) \ america + "\n\n"); ;
  println

  println("[igualdade] America \\ America Sul == America Norte?")
  println(america \ america_sul == america_norte + "\n\n"); ;
  println

  println("[igualdade] America \\ Europa == America?")
  println(america \ europa == america + "\n\n"); ;
  println

  println("[igualdade] America \\ America Sul == America?")
  println(america \ america_sul == america + "\n\n"); ;
  println

  println("[pertence] Brasil E America?")
  println(america E "Brasil");
  println

  println("[pertence] Brasil E Europa?")
  println(europa E "Brasil");
  println

  println("[contido] America c Europa?")
  println(america c europa);
  println

  println("[contido] America Norte c America?")
  println(america_norte c america);
  println

  println("[contido] America Sul c America Norte?")
  println(america_sul c america_norte);
  println

  println("[contar]No elementos Europa")
  println(europa.tamanho() + ": " + europa);
  println

  println("[contar]No elementos America Norte")
  println(america_norte.tamanho() + ": " + america_norte);
  println

  //  var lingua_latina =  new Conjunto[String](m);
  //  lingua_latina.inserir("Franca")
  //  lingua_latina.inserir("Mexico")
  //  lingua_latina.inserir("Brasil")
  //  lingua_latina.inserir("Italia")
  //  lingua_latina.inserir("Argentina")
  //  lingua_latina.inserir("Portugal")
  //  lingua_latina.inserir("Uruguai")
  //
  //  println( america & lingua_latina ) ;
  //  
  //  println( america_sul c america ) ;
  //  
  //  println( america_sul c lingua_latina ) ;
}