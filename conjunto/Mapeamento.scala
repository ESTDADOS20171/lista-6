package conjunto

import scala.collection.mutable.HashMap

class Mapeamento[E >: Null](e:E*) {
  
  private var f = new HashMap[E, Int]();
  private var g = new HashMap[Int, E]();
  private var bits = 1;

  if(e != null) {
    e.foreach { x => this << x };
  }


  
  def <<(e: E): Mapeamento[E] = {
    f.put(e, 1 << f.size);
    g.put(1 << g.size, e);

    bits = (bits << 1) | 1;
    this
  }

  def mask: Int = bits;

  def >(e: E): Int = f.get(e) match { case Some(x) => x; case None => 0 };

  def <(i: Int): E = g.get(i) match { case Some(x) => x; case None => null };
  
  override def toString = f.mkString("{", ",\n", "}");

}